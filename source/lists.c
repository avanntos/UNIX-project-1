#include "../include/lists.h"


/// --- all functions here are pretty standard, as such i believe those don't need an extensive commenting

void add_file_node(filenode* new, filenode** head, pthread_mutex_t* mtx) {

    if(new != NULL) { 
        filenode* itr = new;
        while(itr->next != NULL)
            itr = itr->next;

        // lock
        pthread_mutex_lock(mtx);
        #ifdef __DEGUB__BUILD__LOCKS__
        printf("locked filesnodes\n");
        #endif

        itr->next = *head;
        *head = new;

        // unlock
        pthread_mutex_unlock(mtx);
        #ifdef __DEGUB__BUILD__LOCKS__
        printf("unlocked filesnodes\n");
        #endif
    }
}

filenode* get_file_node(filenode** head, pthread_mutex_t* mtx) {

    // lock
    pthread_mutex_lock(mtx);
    #ifdef __DEGUB__BUILD__LOCKS__
    printf("locked filesnodes\n");
    #endif

    if(*(head) != NULL){

        filenode* ret = *head;
        *head = (*head)->next;
        // head->next = NULL;
        ret->next = NULL;

        // unlock
        pthread_mutex_unlock(mtx);
        #ifdef __DEGUB__BUILD__LOCKS__
        printf("unlocked filesnodes\n");
        #endif

        return ret;
    }

    // unlock
    pthread_mutex_unlock(mtx);
    #ifdef __DEGUB__BUILD__LOCKS__
    printf("unlocked filesnodes\n");
    #endif


    return NULL;
}

void add_match_node(match_records* new, match_records** head, pthread_mutex_t* mtx) {

    if(new != NULL) { 
        match_records* itr = new;
        while(itr->next != NULL)
            itr = itr->next;

        // lock
        pthread_mutex_lock(mtx);
        #ifdef __DEGUB__BUILD__LOCKS__
        printf("locked match\n");
        #endif

        itr->next = *head;
        *head = new;

        // unlock
        pthread_mutex_unlock(mtx);
        #ifdef __DEGUB__BUILD__LOCKS__
        printf("unlocked match\n");
        #endif

    }
}

match_records* get_match_node(match_records* head, pthread_mutex_t* mtx) {
    // lock
    pthread_mutex_lock(mtx);
    #ifdef __DEGUB__BUILD__LOCKS__
    printf("locked match\n");
    #endif

    if(head != NULL){

        match_records* ret = head;
        head = head->next;
        ret->next = NULL;

        // unlock
        pthread_mutex_unlock(mtx);
        #ifdef __DEGUB__BUILD__LOCKS__
        printf("unlocked match\n");
        #endif

        return ret;
    }

    // unlock
    pthread_mutex_unlock(mtx);
    #ifdef __DEGUB__BUILD__LOCKS__
    printf("unlocked match\n");
    #endif

    return NULL;
}
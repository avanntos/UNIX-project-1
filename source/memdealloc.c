#include "../include/memdealloc.h"

void memory_dealoc(match_records* mr) {
    match_records* tmp = mr;

    filenode* prevnode = NULL;

    while(tmp != NULL) {
        /// several record nodes might point to the same file node
        /// but they are always sequential so after we detect new file node pointer
        /// we are safe to delete previous one
        if(prevnode!=NULL && (prevnode != tmp->file)) {
            free(prevnode->path);
            free(prevnode);
        }
        prevnode = tmp->file;

        match_records* todelete = tmp;
        tmp = tmp->next;
        free(todelete);
    }

    /// same deal as in line 13 but for the last object on the list
    if(prevnode != NULL) {
        free(prevnode->path);
        free(prevnode);
    }   
}
#include "../include/search.h"

match_records* search_file(filenode* fnode, const char* phrase, char* buffer) {
    match_records* ret = NULL;
    int length = strlen(phrase);
    // printf("sniffing %s\n", fnode->path);
    int f = open(fnode->path, O_RDONLY);
    int size = lseek(f, 0, SEEK_END);
    lseek(f, 0, SEEK_SET);
    char* find = NULL;
    int l_start, l_end;

    if(size > PAGESIZE) {
        char* tmp_bfr = malloc(sizeof(char)*(size + 1));
        if(tmp_bfr == NULL) {
            perror("Could not allocate more memory. Exiting...\n");
            exit(13);
        }
        find = tmp_bfr;
        read(f, tmp_bfr, size);

        do {
            // printf("searching1\n");
            char* startsearch = tmp_bfr + (find - tmp_bfr);
            if(startsearch > (tmp_bfr + size))
                startsearch = tmp_bfr + size;
            find = strstr(startsearch, phrase);
            if(find != NULL){
                // printf("%s\n", find);
                l_start = find - tmp_bfr;
                l_end = l_start + length;
                if(l_end >= size)
                    l_end = size - 1;
                int count = 3;
                while(l_start>0) {
                    if(tmp_bfr[l_start] == '\n') {
                        // printf("found \\n\n");
                        --count;
                    }
                    if(count == 0){
                        ++l_start;
                        break;
                    }
                    // printf("decrementing l_start\n");
                    --l_start;
                }
                count = 3;
                while(l_end<size) {
                    if(tmp_bfr[l_end] == '\n') {
                        --count;
                    }
                    if(count == 0){
                        --l_end;
                        break;
                    }
                    ++l_end;
                }


                match_records* new_rec = malloc(sizeof(match_records));
                if(new_rec == NULL) {
                    perror("Could not allocate more memory. Exiting...\n");
                    exit(13);
                }
                new_rec->next = ret;
                new_rec->file = fnode;
                new_rec->first_line = l_start;
                new_rec->last_line = l_end;
                ret = new_rec;
                
                // printf("\n----\n");
                // fwrite(tmp_bfr + l_start, 1, l_end - l_start, stdout);
                // printf("\n----\n");
            
                find += length;
            }
        } while(find != NULL);

        free(tmp_bfr);
    } else {

        find = buffer;
        read(f, buffer, size);
        do {
            // printf("searching2\n");
            char* startsearch = buffer + (find - buffer);
            if(startsearch > (buffer + size))
                startsearch = buffer + size - 1;
            find = strstr(startsearch, phrase);
            if(find != NULL){
                // printf("found\n%s\n", find);
                l_start = find - buffer;
                l_end = l_start + length;
                if(l_end >= size)
                    l_end = size - 1;
                int count = 3;
                while(l_start>0) {
                    if(buffer[l_start] == '\n') {
                        --count;
                    }
                    if(count == 0){
                        ++l_start;
                        break;
                    }
                    --l_start;
                }
                count = 3;
                while(l_end<size) {
                    if(buffer[l_end] == '\n') {
                        --count;
                    }
                    if(count == 0){
                        --l_end;
                        break;
                    }
                    ++l_end;
                }


                match_records* new_rec = malloc(sizeof(match_records));
                if(new_rec == NULL) {
                    perror("Could not allocate more memory. Exiting...\n");
                    exit(13);
                }
                new_rec->next = ret;
                new_rec->file = fnode;
                new_rec->first_line = l_start;
                new_rec->last_line = l_end;
                ret = new_rec;
                // printf("\n----\n");
                // fwrite(buffer + l_start, 1, l_end - l_start, stdout);
                // printf("\n----\n");
            
                find += length;
            }
        } while(find != NULL);

    }

    close(f);

    return ret;

}

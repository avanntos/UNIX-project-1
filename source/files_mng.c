#include "../include/files_mng.h"


char check_filename(char* filename, int n, const char** ex_extensions, int ex_size) {

    /// check for obvious filenames to exclude
    if(strcmp(filename, ".") == 0 || strcmp(filename, "..") == 0) {
        return -2;
    }
    
    /// looking for where the last dot is in the filename, marking the extention
    int dotindex = -1;
    for(int i=n; i>=0; --i) {
        if(filename[i] == '.') {
            dotindex = i;
            break;
        }
    }

    // no extension found
    if(dotindex == -1)
        return -1;

    // dotfile, assuming text file
    if(dotindex == 0)
        return 0;

    // hardcoded default extensions
    char* extensions[] = {".cfg", ".c", ".h", ".cpp", ".hpp", ".txt", ".ini"};
    for(int i=0; i<sizeof(extensions)/sizeof(char*); ++i) {

        if((n-dotindex) == strlen(*(extensions+i)))
            /// remaining length from before the dot is the same as length of the extension
            /// we check if it's matching one
            for(int j=0; (dotindex + j)<=n; ++j) {
                if(filename[dotindex + j] != *(*(extensions+i)+j))
                    break;
                    /// one of character does not match, try next one

                if((j + dotindex) == n)
                    return 0;
                    /// we got to the end, all letters matched
            }
    }

    /// additional extensions, passed by user
    for(int i=0; (ex_extensions!= NULL) && (i<=ex_size); ++i) {
        if((n-dotindex) == strlen(*(ex_extensions+i)))
            /// remaining length from before the dot is the same as length of the extension
            /// we check if it's matching one
            for(int j=0; (dotindex + j)<=n; ++j) {
                if(filename[dotindex + j] != *(*(ex_extensions+i)+j))
                    break;
                    /// one of character does not match, try next one

                if((j + dotindex) == n)
                    return 0;
                    /// we got to the end, all letters matched
            }
    }
    
    return -1;
}



filenode* mk_list(char* root, filenode* head, const char** extensions, int ext_size) {
    if(access(root, R_OK) != 0) {
        // printf("Could not open directory: %s", root);
        return NULL;
    }

    DIR* dir = opendir(root);
    if(dir == NULL) {
        perror("Could not open directory.");
        exit(14);
    }
    filenode* directory_list = head;
    struct dirent* dirlist = 0;

    char* fulldirpath = malloc(256*sizeof(char));
    if(fulldirpath == NULL) {
        perror("Could not allocate more memory. Exiting...\n");
        exit(13);
    }

    realpath(root, fulldirpath);
    strcat(fulldirpath, "/");

    while((dirlist = readdir(dir)) != NULL) {
        int strlength = strlen(dirlist->d_name);
        
        int file_status = 0;
        chdir(fulldirpath);
        if(access(dirlist->d_name, R_OK) != 0) {
            chdir("..");
            // printf("Could not open file [%s%s]\n", fulldirpath, dirlist->d_name);
            continue;
        }
        file_status = check_filename(dirlist->d_name, strlength, extensions, ext_size);

        /// file with wrong filename
        if((dirlist->d_type == DT_REG) && (file_status == -1)) {
            continue;
        }

        /// directory, either "." or ".."
        if((dirlist->d_type == DT_DIR) && file_status == -2) {
            continue;
        }
        
        char* path = getfullpath(fulldirpath, dirlist->d_name);
        struct stat st;
        lstat(path, &st);

        /// symlinks are ignored
        if((dirlist->d_type == DT_DIR) && S_ISLNK(st.st_mode)) {
            continue;
        }

        filenode* node = malloc(sizeof(filenode));
            if(node == NULL) {
            perror("Could not allocate more memory. Exiting...\n");
            exit(13);
        }
        
        /// file is a directory, we mark it so
        if(dirlist->d_type == DT_DIR) {
            node->dir = 1;
        } else {
            node->dir = 0;
        }
        
        /// assign path
        node->path = path;

        /// add to the list
        node->next = directory_list;
        directory_list = node;
    }
    // free(dir);
    closedir(dir);
    free(fulldirpath);
    return directory_list;
}
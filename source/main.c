#include"../include/structs.h"
#include"../include/threads.h"
#include"../include/utility.h"
#include"../include/search.h"
#include"../include/print.h"
#include"../include/memdealloc.h"
#include"../include/lists.h"
#include"../include/defines.h"
#include"../include/files_mng.h"


int main(int argc, char** argv) {

    opterr = 0;

    int c;
    char* exval = NULL;
    while ((c = getopt (argc, argv, ":e:")) != -1) {
        switch (c) {
        case 'e':
            exval = optarg;
            break;
        case '.':
            break;
        case ':':
            printf("Option needs a value!\n");
            break;
        case '?':
            printf("Unknown option character '%c'.\n", optopt);
            return -1;
        default:
            prnt_help();
            return -1;
        }
    }

    if((argc - optind) != 2){
        prnt_help();
        return -1;
    }
    char* path = argv[optind];
    char* phrase = argv[optind+1];

    struct stat test;
    stat(path, &test);
    if(!S_ISDIR(test.st_mode)) {
        printf("Error, argument is not a directory!\n");
        return -1;
    }

    int ext_size = 0;
    const char** extensions = (const char**) split_string(" ", exval, &ext_size);



    filenode* n = mk_list(*(argv+optind), NULL, extensions, ext_size);

    // printf("***files***\n");
    // filenode* tmp = n;
    // while(tmp!=NULL) {
    //     printf("%s\n", tmp->path);

    //     tmp = tmp->next;
    // }
    // printf("***files***\n");

    pthread_mutex_t mutex_file_node = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex_record_node = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mutex_thread_state = PTHREAD_MUTEX_INITIALIZER;

    thread_data* td = malloc(sizeof(thread_data));
    if(td == NULL) {
        perror("Could not allocate more memory. Exiting...\n");
        exit(13);
    }
    td->extensions = extensions;
    td->extensions_size = ext_size;
    td->f_node=n;
    td->m_record=NULL;
    td->searched_phrase=phrase;
    td->idle_threads=THREAD_POOL;
    td->mutex_f_node = &mutex_file_node;
    td->mutex_m_record = &mutex_record_node;
    td->mutex_idle_threads = &mutex_thread_state;

    pthread_t threads[THREAD_POOL];

    for(int i=0; i<THREAD_POOL; ++i) {
        pthread_create(threads + i, NULL, thread_worker, (void*)td);
    }

    for(int i=0; i<THREAD_POOL; ++i) {
        pthread_join(*(threads + i), NULL);
    }

    print_results(td->m_record);

    memory_dealoc(td->m_record);
    free(td);
}
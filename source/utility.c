#include "../include/utility.h"

char* getfullpath(char* root, char* file) {
    char *ret = malloc(strlen(root) + strlen(file) + 1);
    if(ret == NULL) {
        perror("Could not allocate more memory. Exiting...\n");
        exit(13);
    }
    strcpy(ret, root);
    strcat(ret, file);
    return ret;
}

char** split_string(char* del, char* str, int* x_size) {
    if((del == NULL) || (str == NULL))
        return NULL;
    int c = 0;
    for(int i=0; i<strlen(str); ++i) {
        if(str[i] == *del)
            ++c;
    }
    *x_size = c;
    char** ret = malloc(c*sizeof(char*));
    if((c!=0) && ret == NULL) {
        perror("Could not allocate more memory. Exiting...\n");
        exit(13);
    }

    char* ch;
    ch = strtok(str, del);
    int i=0;
    while(ch != NULL) {
        // printf ("%s\n", ch);
        *(ret+i) = malloc(strlen(ch + 1));
        if(*(ret+i) == NULL) {
            perror("Could not allocate more memory. Exiting...\n");
            exit(13);
        }
        strcpy(*(ret+i), ch);
        ++i;
        ch = strtok (NULL, del);
    }
    return ret;
}

void prnt_help(void) {
    printf("fcrawler by Lukasz Idzik\n");
    printf("\tusage:\n");
    printf("\t\tfirst argument - directory/file path,\n");
    printf("\t\tsecond argument - searched phrase,\n");
    printf("\t\t-e - adding additional extensions (multiple extensions enclosed in quote),\n");

    printf("\nExample:\tfcrawler path phrase -e \".some .extensions\"\n");
    return;   
}
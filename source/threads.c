#include "../include/threads.h"

void change_state(int s, int* th_s, pthread_mutex_t* mtx) {
    /// lock
    pthread_mutex_lock(mtx);
    // printf("locked state\n");

    *th_s += s;

    /// unlock
    pthread_mutex_unlock(mtx);
    // printf("unlocked state\n");

}

int check_threads_state(int* th_s, pthread_mutex_t* mtx) {
    /// lock
    pthread_mutex_lock(mtx);
    #ifdef __DEGUB__BUILD__LOCKS__
    printf("locked state\n");
    #endif

    int ret = *th_s;

    /// unlock
    pthread_mutex_unlock(mtx);
    #ifdef __DEGUB__BUILD__LOCKS__
    printf("unlocked state\n");
    printf("state is %d\n", ret);
    #endif

    return ret;
}

void* thread_worker(void* data) {
    thread_data* t_data = (thread_data*) data;

    /// allocating buffer used by this thread for checking files' content
    char* _buffer_ = malloc(PAGESIZE*sizeof(char));
    if(_buffer_ == NULL) {
        perror("Could not allocate more memory. Exiting...\n");
        exit(13);
    }

    // state: 1 - working, 0 - idle
    int state = 1;
    /// if thread counter is greater than zero - some threads are still at work,
    /// and might add some more work to do for other threads
    while((check_threads_state(&(t_data->idle_threads), t_data->mutex_idle_threads) != 0) && (t_data->f_node != NULL)){
        /// pick filenode we work on during this iteration
        filenode* fn = get_file_node(&(t_data->f_node), t_data->mutex_f_node);

        /// if we get NULL value - no work for now
        if(fn != NULL) {
            /// state of thread changed, we can mark ourselves as active again, add one to to counter
            if(state == 0) {
                state = 1;
                change_state(1, &(t_data->idle_threads), t_data->mutex_idle_threads);
            }

            #ifdef __DEGUB__BUILD__NODES__
            printf("worker: node: %s\n", fn->path);
            #endif

            if(fn->dir == 1) {
                /// if it's directory we scan it for more files
                filenode* new_list = mk_list(fn->path, NULL, t_data->extensions, t_data->extensions_size);

                /// and add them to the list to process them
                add_file_node(new_list, &(t_data->f_node), t_data->mutex_f_node);

                /// don't need this node, deallocate
                free(fn->path);
                free(fn);
            } else {
                /// wipe buffer, i got a bug somewhere that messes up the output and this seems to fix it
                for(int i=0; i<PAGESIZE; ++i) {
                    _buffer_[i] = '\0';
                }

                /// search file for records
                match_records* new_list = search_file(fn, t_data->searched_phrase, _buffer_);

                /// if no records found, we don't need this filenode
                if(new_list == NULL) {
                    free(fn->path);
                    free(fn);
                } else {
                    /// records found, add them to records list
                    add_match_node(new_list, &(t_data->m_record), t_data->mutex_m_record);
                }
            }
        } else {
            /// no work - we mark as idle and update the counter (this one ONLY when state changed this iteration)
            if(state != 0)
                change_state(-1, &(t_data->idle_threads), t_data->mutex_idle_threads);
            state = 0;
            
            /// no work to do so might as well give up CPU time
            sched_yield();
        }
    }

    /// dealloc buffer
    free(_buffer_);

    pthread_exit(NULL);

}

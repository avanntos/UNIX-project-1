#include "../include/print.h"

void print_results(match_records* mr) {
    match_records* itr = mr;

    char* _buffer_ = malloc(PAGESIZE*sizeof(char));
    if(_buffer_ == NULL) {
        perror("Could not allocate more memory. Exiting...\n");
        exit(13);
    }

    while(itr != NULL) {
        int l_start = itr->first_line;
        int l_end = itr->last_line;

        int file = open((itr->file)->path, O_RDONLY);

        lseek(file, l_start, SEEK_SET);

        read(file, _buffer_, l_end - l_start);

        printf("\nFile:[%s].\n...\n", (itr->file)->path);
        fwrite(_buffer_, 1, l_end - l_start, stdout);
        printf("\n...\n");

        close(file);
        itr = itr->next;
    }

    free(_buffer_);
}
#ifndef _STRUCTS_H_
#define _STRUCTS_H_

#include<sys/types.h>

/// --- structure storing file information, next node, information whether it's a directory and it's path
typedef struct filenode {
    struct filenode* next;
    int dir;
    char* path;
} filenode;

/// --- structure storing record's data, next node, file where record is found, and where to start/end printing it
typedef struct match_records {
    struct match_records*   next;
    filenode*               file;
    int                     first_line;
    int                     last_line;
} match_records;

/// --- structure storing threads' data, searched phrase, extensions passed by user, list of filenodes, list of record nodes
///     mutexes and active thread counter for the thread finish condition
typedef struct thread_data {
    const char*         searched_phrase;
    const char**        extensions;
    int                 extensions_size;
    filenode*           f_node;
    pthread_mutex_t*    mutex_f_node;   
    match_records*      m_record;
    pthread_mutex_t*    mutex_m_record;   
    int                 idle_threads;
    pthread_mutex_t*    mutex_idle_threads;  
} thread_data;

#endif
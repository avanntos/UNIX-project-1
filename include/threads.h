#ifndef _THREADS_H_
#define _THREADS_H_

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sched.h>

#include "defines.h"
#include "search.h"
#include "structs.h"
#include "lists.h"
#include "files_mng.h"

/// --- changes state of the thread
///     accepts as arguments:
///         integer to add to counter available for all threads,
///             when counter falls to 0, it means all threads are idle and it's safe to quit
///         pthread_mutex_t* pointer to mutex, since all threads can change counter value, it needs to be protected
void change_state(int s, int* th_s, pthread_mutex_t* mtx) ;

/// --- checks state of other threads to see if it's safe to quit
///     accepts as arguments:
///         int* pointer to counter storing the active thread counter value
///         pthread_mutex_t* pointer to mutex, since all threads can change counter value, it needs to be protected
int check_threads_state(int* th_s, pthread_mutex_t* mtx) ;

/// --- runnable threads
///     accepts an an argument void pointer to thread_data structure with all data needed to run its task
///     threads is basically on a constant loop checking whether there is any work to do
///     thread finishes when it and all other threads are marked as idle (all work is done)
void* thread_worker(void* data) ;


#endif
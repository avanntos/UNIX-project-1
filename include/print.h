#ifndef _PRINT_H_
#define _PRINT_H_

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include"structs.h"
#include"defines.h"

void print_results(match_records* mr) ;

#endif
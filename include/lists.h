#ifndef _LISTS_H_
#define _LISTS_H_

#include<pthread.h>

#include"structs.h"
#include"defines.h"

/// --- simple function adding file node to the list
///     accepted arguments:
///         filenode* pointer to new node
///         filenode** pointer to head node
///         pthread_mutex_t* pointer
///     uses passed mutex to lock variable when needed
void add_file_node(filenode* new, filenode** head, pthread_mutex_t* mtx) ;

/// --- simple function for retrieving file node from the list
///     accepted arguments:
///         filenode** pointer to head node
///         pthread_mutex_t* pointer
///     uses passed mutex to lock variable when needed
filenode* get_file_node(filenode** head, pthread_mutex_t* mtx) ;

/// --- simple function adding record node to the list
///     accepts:
///         match_record* pointer to new node
///         match_record** pointer to head node
///         pthread_mutex_t* pointer
///     uses passed mutex to lock variable when needed
void add_match_node(match_records* new, match_records** head, pthread_mutex_t* mtx) ;

/// --- simple function for retrieving record node from the list
///     accepted arguments:
///         filenode** pointer to head node
///         pthread_mutex_t* pointer
///     uses passed mutex to lock variable when needed
match_records* get_match_node(match_records* head, pthread_mutex_t* mtx) ;

#endif
#ifndef _FILESMNG_H_
#define _FILESMNG_H_

#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#include "structs.h"
#include "defines.h"
#include "utility.h"

filenode* mk_list(char* root, filenode* head, const char** extensions, int ext_size) ;

char check_filename(char* filename, int n, const char** ex_extensions, int ex_size) ;

#endif
#ifndef _DEFINES_H_
#define _DEFINES_H_

/// --- BOTH DEFINES JUST FOR TESTING, STDIO NOT THREAD SAFE
/// print out locks activity
// #define __DEGUB__BUILD__LOCKS__

/// print out when thread starts working on new file
// #define __DEGUB__BUILD__NODES__

/// size of buffers and such
#define PAGESIZE 8192

/// number of threads we spawn
#define THREAD_POOL 4

#endif
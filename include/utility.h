#ifndef _UTILITY_H_
#define _UTILITY_H_

#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#include"defines.h"
#include"structs.h"

/// --- function for getting full file path
///     accepts as arguments:
///         char* pointer to absolute parent directory path
///         char* pointer to filename
///
///     strings get concatedated and returned. no memory management on old file name i performed
char* getfullpath(char* root, char* file) ;

/// --- prints out file help information
void prnt_help(void) ;

/// --- function splits string into multiple strings based on delimiter passed
///     accepts as arguments:
///         char* pointer to string ith delimiter used
///         char* pointer to string which will be split
///         int* pointer, function will store amount of strings returned as a result
/// 
///     returns array of strings
char** split_string(char* del, char* str, int* x_size) ;

#endif
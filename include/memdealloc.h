#ifndef _MEMDEALLOC_H_
#define _MEMDEALLOC_H_
#include<stdlib.h>

#include"structs.h"
#include"defines.h"

/// function taking care of deallocating match_records list of objects it accepts as an argument
void memory_dealoc(match_records* mr) ;

#endif
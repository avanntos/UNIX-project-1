#ifndef _SEARCH_H_
#define _SEARCH_H_

#include <unistd.h>
#include <string.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>

#include"defines.h"
#include"structs.h"

/// --- function searching for phrase in a file
///     accepted arguments:
///         filenode* pointer, carries information where to look
///         char* pointer to phrase we are looking for
///         char* pointer to the buffer
///
///
///     i think it's the worst piece of code i've ever written but i just can't figure out a better way of doing it
/// 
///     essentially we get a buffer of fixed side, if we detect file is bigger than the buffer, we allocate new, bigger buffer,
///     which is later deallocated. i meant to read file chunk by chunk and check each individual chunk for matches but..
///     i learned i need to play around more with c strings in the future to practice them
match_records* search_file(filenode* fnode, const char* phrase, char* buffer) ;

#endif
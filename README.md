# Crawl

Simple program which looks for a given string of text in files within given directory tree.
It was a project for Unix programming classes which aimed to utilize C programming language combined with UNIX system calls.
Crawl creates a list of files to check and then proceeds to spawn N threads to do the lookup work within those files.

# TODO:
- fix how the symlinks are handled  (to avoid a little bit of an infinite loop that currently would happen in case of cycling dir tree graphs)
